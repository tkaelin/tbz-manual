# Nutzungsrichtlinie für TBZ Cloud der Technischen Berufsschule Zürich <!-- omit in toc -->

## 1. Allgemeine Bestimmungen

Die Technische Berufsschule Zürich stellt dem Personal und den Lernenden Informatikmittel und Services für den Unterricht zur Verfügung. 
Diese Richtlinie bezweckt, den Benutzenden verständliche und nachvollziehbare Vorgaben zum korrekten Umgang mit Informatikmittel zu geben. Diese Vorgaben regeln die Datensicherheit, den Datenschutz und den Umgang mit Informatikmitteln im schulischen Kontext.

## 2. Geltungsbereich

Die nachfolgenden Regelungen gelten für die gesamte Schule (TBZ) und für alle Benutzenden unabhängig ob mit TBZ-Geräten oder privaten Geräten gearbeitet und allenfalls ein fremdes Netz verwendet wird. Diese Nutzungsrichtlinie gilt für Mitarbeitende, Lehrpersonen, Lernende, Studenten, die Zugang zu Informatikmittel der TBZ haben.
Die Benutzenden sind persönlich dafür verantwortlich, diese Richtlinie einzuhalten. Mit der ersten Anmeldung oder der Nutzung der zur Verfügung gestellten Informatikmittel nehmen die Benutzenden die Nutzungsrichtlinie zur Kenntnis und bestätigen, über die Konsequenzen bei deren Nichtbeachtung informiert worden zu sein.
Diese Nutzungsrichtlinie ist ergänzend zu der Richtlinie [Nutzungsrichtlinie für Informatikmittel an der Technischen Berufsschule Zürich (D5.4-03E)](https://orgweb.tbz.ch/media/o45bhdii/d54-03.pdf). 

## 3. Nutzung von Informatikmittel und TBZ Cloud

An der Technischen Berufsschule Zürich (TBZ) werden Informatikmittel verwendet, die von der Schule und / oder vom Kanton Zürich bereitgestellt bzw. verwaltet werden. Die Informatikmittel umfassen alle Systeme, inkl. Netzwerk und Zugang zum Internet sowie
Programme oder Applikationen, die von der Schule zur Verfügung gestellt werden. 
Diese Nutzungsrichtlinie regelt insbesondere die Benutzung der TBZ Cloud. 
Die Benutzenden behandelt die Services mit Sorgfalt und treffen alle notwendigen Massnahmen um die Services vor möglichen Schaden zu schützen. 

## 4. Lizenzbestimmungen für Software

Es gelten die Regeln gemäss Abschnitt 3.1 der *Nutzungsrichtlinie für Informatikmittel an der Technischen Berufsschule Zürich (D5.4-03E)*. 

## 5. Unerlaubte Handlungen und Massnahmen bei Verstössen

Grundsätzlich gilt, dass die Services ausschliesslich für den im Unterrichten bestimmten Zweck zu verwenden sind. Die Nutzung der Services für andere Zwecke ist untersagt.  

Es gelten die Regeln gemäss Abschnitt 7 der *Nutzungsrichtlinie für Informatikmittel an der Technischen Berufsschule Zürich (D5.4-03E)*. 
Dazu gilt insbesondere: 

Wenn die Services die Speicherung von Daten durch den Benutzer erlauben, so ist es untersagt Personenbezogene Daten abzuspeichern.

Beispiel: Nicht erlaubt ist eine Liste mit Namen der Klassenkollegen als Datei auf einem virtuell Server zu speichern. 

## 6. Haftung und Haftausschluss
Die Benutzenden haften für von ihr/ihm vorsätzlich oder grobfahrlässig verursachte Schäden oder Veränderungen an Informatikmitteln der TBZ oder für mit Informatikmitteln der TBZ verursachte Schäden innerhalb und ausserhalb der Schule. Die Schäden, beziehungsweise deren Beseitigung, werden dem Verursacher in Rechnung gestellt. Soweit die Rechtsordnung dies zulässt, schliesst die Schule jede Haftung für Schäden durch Benutzerhandlungen aus. Die Schule haftet ausserdem nicht für Schäden, die den Benutzenden aus ihrer Missachtung dieser Nutzungsrichtlinie und des anwendbaren Datenschutzrechts sowie der Missachtung der kantonalen AISR (Allgemeine Informationssicherheitsrichtlinie) und anwendbaren BISR (Besonderen Informationssicherheitsrichtlinien) entstehen.

## 7. Datenschutz

### 7.1. Erhobene Daten
Solange sich Benutzer die TBZ Cloud nicht nutzen, werden keine Daten über diesen in Klartext gespeichert. 

Loggt sich eine Benutzer*in ein oder werden folgende Daten gespeichert:
 - E-Mail-Adresse
 - Zugehörige Kürzel der Klasse
 - IP-Adresse

### 7.2. Zweck
 - **E-Mail-Adresse**: Versand der Zugangsdaten, Unterstützung des Benutzers bei Störungen, Identifikation der zugehörigen Sitzungen
 - **Kürzel der Klasse**: Organisationseinheit, Benutzer werden Klassenweise freigegeben
 - **IP-Adresse**: Zur Rückverfolgung bei unerlaubten Handlungen

### 7.3. Schutz der Daten
Die Technischen Berufsschule Zürich verfügt über angemessene, dem aktuellen Stand der Technik entsprechende Sicherheitsvorkehrungen zum Schutz der Personendaten vor unberechtigtem Zugriff und Missbrauch. Solche Sicherheitsvorkehrungen umfassen technische Massnahmen zur Cybersicherheit wie IT- und Netzwerksicherheitslösungen, die Verschlüsselung der Daten und die Sicherstellung des Need-to-Know beim Zugriff auf Ihre Daten sowie organisatorische und administrative Massnahmen wie der Sicherstellung einer regelmässigen Überprüfung der Kontrollen, der Schulung von Mitarbeitenden und ein Regelwerk von einschlägigen Weisungen.

### 7.4. Weitergabe von Daten
Es werden keine Daten weitergeben. 

Ausnahme ist die Weitergabe von Daten bei strafrechtlichen Handlungen an die entsprechenden Behörden. 

### 7.5. Datenschutzrechte der Betroffenen
Benutzer sind jederzeit berechtigt, Ihre Datenschutzrechte geltend zu machen, insbesondere Auskunft über Ihre bei uns gespeicherten Daten oder die Berichtigung, Ergänzung oder Löschung Ihrer Personendaten zu verlangen, sofern Ihrem Anliegen keine gesetzlichen Bearbeitungspflichten entgegenstehen.
